import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Menu");
        System.out.println("1. Say hello");
        System.out.println("2. Say Bay");
        System.out.println("3. Say Exit");

        Scanner in = new Scanner(System.in);

        int command = in.nextInt();

        switch (command) {
            case 1: {
                System.out.println("Hello Dolly");
                break;
            }
            case 2: {
                System.out.println("Bay");
                break;
            }
            case 3: {
                System.out.println("Exit");
                break;
            }

        }
    }
}